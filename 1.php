<!DOCTYPE html>
<html class="no-js" lang="es">
<head>
	<meta charset="utf-8">
	<link rel="stylesheet" href="estilos.css"> 
</head>
	<body>
		<hr>
		<h1>Tabla del 1 al 100</h1>
		<p>matriz 10 x 10</p>
		<hr>
		<table class="center">
		<?php
			//se le asigna el tamaño para la tabla del 1 al 100
			$tamano = 10;
			//variable que se va a imprimir
			$z = 0;
			//se empieza a crear la tabla con un espacio en blanco
			for ($x = 1;$x <= $tamano; $x++ ) {
				//se crea la primera fila de la tabla
				echo ("<tr>");
				//se hace un segundo for para ir rellenando las columnas	 
				for ($y = 1;$y <= $tamano; $y++ ) {
					$z = $z+1;
					//se habre la primera columna
					echo ("<td>"); 
					echo ("$z");
					//se cierra la primera columna
					echo ("</td>"); 
				}
				echo ("</tr>\n");
			}
		?>
		</table>
	</body>
</html>
