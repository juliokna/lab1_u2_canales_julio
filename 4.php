<!DOCTYPE html>
<html class="no-js" lang="es">
<head>
	<meta charset="utf-8">
</head>
	<body>
		<?php

		//indicamos la ruta
		$carpeta= "/home/jkna/Imágenes";

		//Abrimos la ruta que hemos indicado
		$directorio = opendir($carpeta);

		while ($archivo = readdir($directorio)) {
		$imagen = GetImageSize($carpeta . $archivo);
		?>

		<!–aca le coloco por ejemplo 600px X 350px de dimensiones–>
		<img src=»<?php echo $carpeta.$archivo ?>» width=»500″ height=»250″>
		<br>
		<?php
		
		}

		//cerramos el directorio luego de la búsqueda del archivo de imagen
		closedir($directorio);
		?>
	</body>
</html>

/**
 * Funcion que muestra la estructura de carpetas a partir de la ruta dada.
 */

function obtener_estructura_directorios($ruta){
    // Se comprueba que realmente sea la ruta de un directorio
    if (is_dir($ruta)){
        // Abre un gestor de directorios para la ruta indicada
        $gestor = opendir($ruta);
        echo "<ul>";

        // Recorre todos los elementos del directorio
        while (($archivo = readdir($gestor)) !== false)  {
                
            $ruta_completa = $ruta . "/" . $archivo;

            // Se muestran todos los archivos y carpetas excepto "." y ".."
            if ($archivo != "." && $archivo != "..") {
                // Si es un directorio se recorre recursivamente
                if (is_dir($ruta_completa)) {
                    echo "<li>" . $archivo . "</li>";
                    obtener_estructura_directorios($ruta_completa);
                } else {
                    echo "<li>" . $archivo . "</li>";
                }
            }
        }
        
        // Cierra el gestor de directorios
        closedir($gestor);
        echo "</ul>";
    } else {
        echo "No es una ruta de directorio valida<br/>";
    }
}
