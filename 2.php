<!DOCTYPE html>
<html class="no-js" lang="es">
<head>
	<meta charset="utf-8">
	<link rel="stylesheet" href="estilos.css"> 
</head>
	<body>
		<hr>
		<h1>Tabla con los numeros del 1 al NxN</h1>
		<p>matriz con N = 15</p>
		<hr>
		<table class="center">
		<?php
			//se definen las variables TAM y N
			$TAM = 15;
			$N = 15;
			
			//variable que se va a imprimir
			$z = 0;
			//se empieza a crear la tabla con un espacio en blanco
			for ($x = 1;$x <= $TAM; $x++ ) {
				//se crea la primera fila de la tabla
				echo ("<tr>");
				//se hace un segundo for para ir rellenando las columnas	 
				for ($y = 1;$y <= $N; $y++ ) {
					$z = $z+1;
					//se crean dos condiciones para colorear o no los recuadros
					if($y%2){
						//se habre la columna que debiese pintarse gris 
						echo ("<td style='background-color: rgb(174, 174, 174);'>");
						echo ("$z");
					}
					else{
						//se habre la columna que no debiese pintarse gris
						echo ("<td>");
						echo ("$z");
					}
					//se cierra la primera columna
					echo ("</td>"); 
				}
				echo ("</tr>\n");
			}
		?>
		</table>
	</body>
</html>
