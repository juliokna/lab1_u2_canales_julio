<!DOCTYPE html>
<html class="no-js" lang="es">
<head>
	<meta charset="utf-8">
	<link rel="stylesheet" href="estilos.css"> 
</head>
	<body>
		<hr>
		<h1>Tabla N x N</h1>
		<p>matriz dinamica con el metodo POST</p>
		<hr>
		<form action="" method="post">
		  <label for="TAM">Tamaño:</label>
		  <input type="text" name="TAM"><br><br>
		  <label for="color">Color:</label>
		  <input type="text" name="color"><br><br>
		  <input type="submit" value="Submit">
		</form>
		<hr>
		<table class="center">
		<?php
			//se definen las variables usando metodo GET
			$TAM = $_POST["TAM"];
			$color = $_POST["color"];
			
			//variable que se va a imprimir
			$z = 0;
			//se empieza a crear la tabla con un espacio en blanco
			for ($x = 1;$x <= $TAM; $x++ ) {
				//se crea la primera fila de la tabla
				echo ("<tr>");
				//se hace un segundo for para ir rellenando las columnas	 
				for ($y = 1;$y <= $TAM	; $y++ ) {
					$z = $z+1;
					//se crean dos condiciones para colorear o no los recuadros
					if($y%2){
						//se habre la columna que debiese pintarse  
						echo ("<td style='background:$color'>");
						echo ("$z");
					}
					else{
						//se habre la columna que no debiese pintarse
						echo ("<td>");
						echo ("$z");
					}
					//se cierra la primera columna
					echo ("</td>"); 
				}
				echo ("</tr>\n");
			}
		?>
		</table>
	</body>
</html>

